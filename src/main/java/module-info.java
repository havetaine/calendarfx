module com.kalendarz.kalendarzfx {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.logging;


    opens com.kalendarz.kalendarzfx to javafx.fxml;
    exports com.kalendarz.kalendarzfx;
}