package com.kalendarz.kalendarzfx;

import com.kalendarz.kalendarzfx.simpleCalendar.SimpleCalendar;
import javafx.css.PseudoClass;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.*;

public class InputWindowController
{
    @FXML
    private TextField date_input;
    @FXML
    private Label warning;

    private Stage stage;
    private Scene scene;
    private Parent root;
    private String date;

    public void switchScene(ActionEvent event) throws IOException
    {
        date = date_input.getText();
        if (date == null || date.isEmpty() || !checkDate())
        {
            warning.setVisible(true);
            date_input.pseudoClassStateChanged(PseudoClass.getPseudoClass("error"), true);
            return;
        }
        else
        {
            warning.setVisible(false);
        }

        FXMLLoader loader = new FXMLLoader(getClass().getResource("calendar-window.fxml"));
        root = loader.load();
        CalendarWindowController controller = loader.getController();
        controller.loadCalendar(date);

        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
    }

    public void getDateFromAFile(ActionEvent event)
    {
        FileChooser fileChooser = new FileChooser();

        File selectedFile = fileChooser.showOpenDialog(stage);

        try
        {
            SimpleCalendar sp = SimpleCalendar.getDateFromFile(selectedFile);
            date = sp.getDay() + "-" + sp.getMonth().getMonth() + "-" + sp.getYear();
            date_input.setText(date);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

    }

    private boolean checkDate()
    {
        return date.matches("^(?:(?:31(\\/|-|\\.)(?:0?[13578]|1[02]))\\1|(?:(?:29|30)(\\/|-|\\.)(?:0?[13-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\/|-|\\.)0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\/|-|\\.)(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$");
    }
}
