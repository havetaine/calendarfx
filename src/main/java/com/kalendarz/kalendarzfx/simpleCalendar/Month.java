package com.kalendarz.kalendarzfx.simpleCalendar;

public class Month
{
    private final String name;
    private final int daysInMonth;
    private final int month;

    public final static int NUMERIC_MONTH = 0;
    public final static int ROMAN_MONTH = 1;
    public final static int NAME_OF_THE_MONTH = 2;
    public final static int SHORT_NAME_OF_THE_MONTH = 3;

    private static final Month[] months =
    {
        new Month(31, 1, "Styczeń"),
        new Month(28, 2, "Luty"),
        new Month(31, 3, "Marzec"),
        new Month(30, 4, "Kwiecień"),
        new Month(31, 5, "Maj"),
        new Month(30, 6, "Czerwiec"),
        new Month(31, 7, "Lipiec"),
        new Month(31, 8, "Sierpień"),
        new Month(30, 9, "Wrzesień"),
        new Month(31, 10, "Październik"),
        new Month(30, 11, "Listopad"),
        new Month(31, 12, "Grudzień"),
    };

    private Month(int daysInMonth, int month, String name)
    {
        this.name = name;
        this.daysInMonth = daysInMonth;
        this.month = month;
    }

    /**
     * Metoda zwracająca obiekt typu Month ze statycznej tablicy.
     * @param month numer miesiąca (1 - 12)
     * @return obiekt typu miesiąc
     */
    public static Month getMonthFromArray(int month)
    {
        if(month < 1 || month > 12)
        {
            throw new IncorrectMonthDataException();
        }

        return months[month - 1];
    }

    public String getName()
    {
        return name;
    }

    public int getDaysInMonth()
    {
        return daysInMonth;
    }

    public int getMonth()
    {
        return month;
    }
}