package com.kalendarz.kalendarzfx.simpleCalendar;

import java.io.*;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Klasa implementująca kalendarz wraz ze zmianą daty
 * o jeden tydzień wprzód lub wstecz.
 *
 * @author Michał Baka
 */
public class SimpleCalendar implements Comparable<SimpleCalendar>
{
    private int year;
    private Month month;
    private int day;
    private int dayOfWeek;
    private boolean isLeap;

    // Data referencyjna
    private static final int REF_YEAR = 2020;
    private static final int REF_MONTH = 11;
    private static final int REF_DAY = 30;

    private static final String[] MONTH_IN_ROMAN = { "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII"};
    private static final int[] DAYS_AHEAD = { 0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4 };
    private static final String[] DAYS_OF_WEEK =
    {
        "poniedziałek", "wtorek", "środa", "czwartek", "piątek", "sobota", "niedziela"
    };

    private static final String[] THE_NAMES_OF_THE_MONTHS =
    {
        "styczeń", "luty", "marzec", "kwiecień", "maj", "czerwiec", "lipiec", "sierpień", "wrzesień", "październik",
        "listopad", "grudzień"
    };

    /**
     * Konstruktor obiektu SimpleCalendar.
     * @param year rok.
     * @param month numer miesiąca.
     * @param day dzień miesiąca.
     */
    public SimpleCalendar(int year, int month, int day)
    {
        isLeap = isTheYearLeap(year);

        this.day = day;
        this.month = Month.getMonthFromArray(month);
        this.year = year;
        this.dayOfWeek = -1;
    }

    /**
     * Konstruktor obiektu SimpleCalendar.
     * @param year rok.
     * @param month nazwa miesiąca.
     * @param day dzień miesiąca.
     */
    public SimpleCalendar(String year, String month, String day)
    {
        this.year = Integer.parseInt(year);
        this.month = Month.getMonthFromArray(Arrays.binarySearch(THE_NAMES_OF_THE_MONTHS, month) + 1);
        this.day = Integer.parseInt(day);
        this.dayOfWeek = -1;
    }

    /**
     * Statyczna metoda tworząca obiekt z podanej daty np. 23-5-2021.
     * @param date data w odpowiednim formacie.
     * @return nowy obiekt SimpleCalendar
     */
    public static SimpleCalendar parse(String date)
    {
        String[] splitDate = date.split("[-\\\\./]");

        return new SimpleCalendar(Integer.parseInt(splitDate[2]), Month.getMonthFromArray(Integer.parseInt(splitDate[1])).getMonth(),
                Integer.parseInt(splitDate[0]));
    }

    /**
     * Przesuwa datę o jeden tydzień do przodu.
     */
    public void oneWeekForward()
    {
        int tmpMonth = month.getMonth();

        day += 7;
        if(isLeap && tmpMonth == 2)
        {
            if(day > month.getDaysInMonth() + 1)
            {
                day -= month.getDaysInMonth() + 1;
                tmpMonth++;
            }
        }
        else
        {
            if(day > month.getDaysInMonth())
            {
                day -= month.getDaysInMonth();
                tmpMonth++;
                if(tmpMonth > 12)
                {
                    year++;
                    tmpMonth = 1;
                    isLeap = isTheYearLeap(year);
                }
            }
        }

        if(month.getMonth() != tmpMonth)
        {
            month = Month.getMonthFromArray(tmpMonth);
        }
    }

    /**
     * Przesuwa datę o jeden tydzień wstecz.
     */
    public void oneWeekBack()
    {
        int tmpMonth = month.getMonth();

        day -= 7;
        if(isLeap && tmpMonth == 3)
        {
            if(day < 1)
            {
                tmpMonth--;
                month = Month.getMonthFromArray(tmpMonth);
                day = (month.getDaysInMonth() + 1) + day;
            }
        }
        else
        {
            if(day < 1)
            {
                tmpMonth--;
                if(tmpMonth < 1)
                {
                    year--;
                    tmpMonth = 12;
                    isLeap = isTheYearLeap(year);
                }

                month = Month.getMonthFromArray(tmpMonth);
                day = month.getDaysInMonth() + day;
            }
        }
    }

    /**
     * Sprwadza czy dany rok jest przestępny.
     * @param year rok, który chcemy sprawdzić.
     * @return true jeśli rok jest przestępny w przeciwnym wypadku false.
     */
    private static boolean isTheYearLeap(int year)
    {
        return year % 4 == 0 && year % 100 != 0 || year % 400 == 0;
    }

    /**
     * Pobiera datę z pliku o podanej nazwie i zwraca nowy obiekt SimpleCalendar.
     * @param filename nazwa pliku, z którego pobierana jest data.
     * @return nowy obiekt SimpleCalendar lub null, jeśli obiektu nie dało się utworzyć.
     */
    public static SimpleCalendar getDateFromFile(String filename) throws IOException
    {
        int[] dateData = new int[3];
        try(Scanner scanner = new Scanner(new BufferedReader(new FileReader(filename))))
        {
            scanner.useDelimiter("[-\\\\./]");
            for(int i = 0; scanner.hasNext(); i++)
            {
                if(scanner.hasNextInt())
                {
                    dateData[i] = scanner.nextInt();
                }
                else
                {
                    throw new IOException();
                }
            }
        }
        catch (FileNotFoundException e)
        {
            System.out.println("Nie znaleziono pliku o podanej nazwie.");
            e.printStackTrace();
            return null;
        }

        return new SimpleCalendar(dateData[2], dateData[1], dateData[0]);
    }

    /**
     * Pobiera datę z pliku o podanej nazwie i zwraca nowy obiekt SimpleCalendar.
     * @param file plik, z którego pobierana jest data.
     * @return nowy obiekt SimpleCalendar lub null, jeśli obiektu nie dało się utworzyć.
     */
    public static SimpleCalendar getDateFromFile(File file) throws IOException
    {
        int[] dateData = new int[3];
        try(Scanner scanner = new Scanner(file))
        {
            scanner.useDelimiter("[-\\\\./]");
            for(int i = 0; scanner.hasNext(); i++)
            {
                if(scanner.hasNextInt())
                {
                    dateData[i] = scanner.nextInt();
                }
                else
                {
                    throw new IOException();
                }
            }
        }
        catch (FileNotFoundException e)
        {
            System.out.println("Nie znaleziono pliku o podanej nazwie.");
            e.printStackTrace();

            return null;
        }

        return new SimpleCalendar(dateData[2], dateData[1], dateData[0]);
    }

    /**
     * Metoda obliczająca dzień tygodnia na podstawie daty przechowywanej w obiekcie.
     */
    public int getDayOfWeek()
    {
        if(month.getMonth() < 3)
        {
            year--;
        }

        dayOfWeek = (year + year / 4 - year / 100 + year / 400 + DAYS_AHEAD[month.getMonth() - 1] + day) % 7;

        return dayOfWeek;
    }

    /**
     * Metoda obliczająca dzień tygodnia na podstawie daty referencyjnej.
     */
    public int getDayOfWeekFromReferenceDate()
    {
        SimpleCalendar tmp = new SimpleCalendar(year, month.getMonth(), day);
        SimpleCalendar refDate = new SimpleCalendar(REF_YEAR, REF_MONTH, REF_DAY);

        if(tmp.compareTo(refDate) < 0)
        {
            while (tmp.compareTo(refDate) < 0)
            {
                tmp.oneWeekForward();
            }

            dayOfWeek = tmp.day % refDate.day;
        }
        else
        {
            while (tmp.compareTo(refDate) > 0)
            {
                tmp.oneWeekBack();
            }

            dayOfWeek = (refDate.day == tmp.day) ? 0 : 7 - (refDate.day % tmp.day);
        }

        return dayOfWeek + 1;
    }

    public int getYear()
    {
        return year;
    }

    public Month getMonth()
    {
        return month;
    }

    public int getDay()
    {
        return day;
    }

    public boolean isLeap()
    {
        return isLeap;
    }

    @Override
    public String toString()
    {
        return "SimpleCalendar{" +
                "year=" + year +
                ", month=" + month.getMonth() +
                ", day=" + day +
                ", dayOfWeek=" + dayOfWeek +
                ", isLeap=" + isLeap +
                '}';
    }

    @Override
    public int compareTo(SimpleCalendar o)
    {
        if(year < o.year || (year == o.year && month.getMonth() < o.month.getMonth()) ||
                (year == o.year && month.getMonth() == o.month.getMonth() && day < o.day))
        {
            return -1;
        }
        else if(year == o.year && month.getMonth() == o.month.getMonth() && day == o.day)
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }

    /**
     * Zwraca datę przechowywaną w obiekcie zgodną z formatem podanym przez użytkownika.
     * @param format format daty
     * @param separator separator oddzielający dzień, miesiąc i rok
     * Gdy nie użyto metody do wyliczenia dnia tygodnia, nie zostanie on wypisany niezależnie od wartości argumentu.
     * @param monthStyle w jaki sposób będzie wyświetlany miesiąc<br>
     * <b>Z klasy Month:</b><br>
     * NUMERIC_MONTH - miesiąc reprezentowany za pomocą cyfr arabskich
     * ROMAN_MONTH - miesiąc reprezentowany za pomocą cyfr rzymskich
     * NAME_OF_THE_MONTH  - miesiąc reprezentowany przez jego nazwę
     * SHORT_NAME_OF_THE_MONTH - miesiąc reprezentowany przez jego skróconą nazwę
     */
    public String printDate(String format, String separator, int monthStyle)
    {
        StringBuilder date;
        String tmpMonth;

        switch (monthStyle)
        {
            case 0 -> tmpMonth = Integer.toString(month.getMonth());
            case 1 -> tmpMonth = MONTH_IN_ROMAN[month.getMonth() - 1];
            case 2 -> tmpMonth = THE_NAMES_OF_THE_MONTHS[month.getMonth() - 1];
            case 3 -> tmpMonth = THE_NAMES_OF_THE_MONTHS[month.getMonth() - 1].substring(0, 3);
            default -> throw new IncorrectDateFormatException();
        }

        switch (format)
        {
            case "dd-m-yyyy" -> date = new StringBuilder(day + separator + tmpMonth + separator + year);
            case "m-dd-yyyy" -> date = new StringBuilder(tmpMonth + separator + day + separator + year);
            case "yyyy-m-dd" -> date = new StringBuilder(year + separator + tmpMonth + separator + day);
            default -> throw new IncorrectDateFormatException();
        }

        if (dayOfWeek != -1)
        {
            date.insert(0, DAYS_OF_WEEK[dayOfWeek - 1] + ", ");
        }

        return date.toString();
    }
}