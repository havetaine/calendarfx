package com.kalendarz.kalendarzfx;

import com.kalendarz.kalendarzfx.simpleCalendar.SimpleCalendar;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;

public class CalendarWindowController
{
    @FXML
    private Label date_tv;
    @FXML
    private RadioButton dd_m_yyyy;
    @FXML
    private RadioButton m_dd_yyyy;
    @FXML
    private RadioButton yyyy_m_dd;
    @FXML
    private RadioButton arabic;
    @FXML
    private RadioButton roman;
    @FXML
    private RadioButton the_name_of_the_month;
    @FXML
    private RadioButton short_name;
    @FXML
    private TextField separator_input;

    private SimpleCalendar sp;
    private String dateFormat;
    private String separator = "-";
    private int monthStyle;

    public void loadCalendar(String date)
    {
        sp = SimpleCalendar.parse(date);
        sp.getDayOfWeek();
        getDateStyle(null);
        date_tv.setText(sp.printDate(dateFormat, separator, monthStyle));
    }

    public void weekForward(ActionEvent event)
    {
        sp.oneWeekForward();
        date_tv.setText(sp.printDate(dateFormat, separator, monthStyle));
    }

    public void weekBack(ActionEvent event)
    {
        sp.oneWeekBack();
        date_tv.setText(sp.printDate(dateFormat, separator, monthStyle));
    }

    public void getDateStyle(ActionEvent event)
    {
        if (dd_m_yyyy.isSelected())
        {
            dateFormat = dd_m_yyyy.getText();
        }
        else if (m_dd_yyyy.isSelected())
        {
            dateFormat = m_dd_yyyy.getText();
        }
        else if (yyyy_m_dd.isSelected())
        {
            dateFormat = yyyy_m_dd.getText();
        }

        if (arabic.isSelected())
        {
            monthStyle = 0;
        }
        else if (roman.isSelected())
        {
            monthStyle = 1;
        }
        else if (the_name_of_the_month.isSelected())
        {
            monthStyle = 2;
        }
        else if (short_name.isSelected())
        {
            monthStyle = 3;
        }

        if (separator_input.getText() != null && !separator_input.getText().isEmpty() )
        {
            separator = separator_input.getText();
        }

        date_tv.setText(sp.printDate(dateFormat, separator, monthStyle));
    }
}