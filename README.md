# CalendarFX 📅
**CalendarFX** is a simple calendar app made in Java using JavaFX platform. It was made as a small project for "Techniki Obiektowe i Komponentowe" class ✔️.

## Interface
### Opening window
Window where user has the ability to enter a date. The date can also be imported from a file. \
Accepted date format:  *dd-mm-yyyy*  \
Allowed delimiters:  - \ . /  

![](images/input_window.jpg)  

The error message is displayed if the provided date is incorrect.  

![](images/validation.jpg)  


### Editor
Window where user can update: 
* date format,
* month format,
* delimiter,
* change date to one day before or after.


![](images/main_window.jpg)  


## Technology stack
- Java 17
- JavaFX 17.0.1